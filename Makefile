all:
	$(MAKE) -C src all
	if [ ! -d bin ]; then mkdir bin; fi
	if [ ! -d bin/Rz_compiler ]; then mkdir bin/Rz_compiler; fi
	if [ ! -d bin/Rz_compiler/backend ]; then mkdir bin/Rz_compiler/backend; fi
	if [ ! -d bin/Rz_compiler/errors ]; then mkdir bin/Rz_compiler/errors; fi
	if [ ! -d bin/Rz_compiler/frontend ]; then mkdir bin/Rz_compiler/frontend; fi
	if [ ! -d bin/Rz_compiler/backend/allocation ]; then mkdir bin/Rz_compiler/backend/allocation; fi
	if [ ! -d bin/Rz_compiler/backend/codegen ]; then mkdir bin/Rz_compiler/backend/codegen; fi
	if [ ! -d bin/Rz_compiler/backend/controlflow ]; then mkdir bin/Rz_compiler/backend/controlflow; fi
	if [ ! -d bin/Rz_compiler/backend/instructions ]; then mkdir bin/Rz_compiler/backend/instructions; fi
	if [ ! -d bin/Rz_compiler/backend/interference ]; then mkdir bin/Rz_compiler/backend/interference; fi
	if [ ! -d bin/Rz_compiler/backend/operands ]; then mkdir bin/Rz_compiler/backend/operands; fi
	if [ ! -d bin/Rz_compiler/frontend/semantics ]; then mkdir bin/Rz_compiler/frontend/semantics; fi
	if [ ! -d bin/Rz_compiler/frontend/syntax ]; then mkdir bin/Rz_compiler/frontend/syntax; fi
	if [ ! -d bin/Rz_compiler/frontend/semantics/identifier ]; then mkdir bin/Rz_compiler/frontend/semantics/identifier; fi
	if [ ! -d bin/Rz_compiler/backend/instructions/arithmetic_logic ]; then mkdir bin/Rz_compiler/backend/instructions/arithmetic_logic; fi
	if [ ! -d bin/Rz_compiler/backend/instructions/branch_jump ]; then mkdir bin/Rz_compiler/backend/instructions/branch_jump; fi
	if [ ! -d bin/Rz_compiler/backend/instructions/comparison ]; then mkdir bin/Rz_compiler/backend/instructions/comparison; fi
	if [ ! -d bin/Rz_compiler/backend/instructions/load_store_move ]; then mkdir bin/Rz_compiler/backend/instructions/load_store_move; fi
	if [ ! -d bin/Rz_compiler/backend/instructions/visitors ]; then mkdir bin/Rz_compiler/backend/instructions/visitors; fi
	

	cp src/Rz_compiler/*.class bin/Rz_compiler/
	cp src/Rz_compiler/errors/*.class bin/Rz_compiler/errors/
	cp src/Rz_compiler/backend/allocation/*.class bin/Rz_compiler/backend/allocation/
	cp src/Rz_compiler/backend/codegen/*.class bin/Rz_compiler/backend/codegen/
	cp src/Rz_compiler/backend/controlflow/*.class bin/Rz_compiler/backend/controlflow/
	cp src/Rz_compiler/backend/instructions/*.class bin/Rz_compiler/backend/instructions/
	cp src/Rz_compiler/backend/interference/*.class bin/Rz_compiler/backend/interference/
	cp src/Rz_compiler/backend/operands/*.class bin/Rz_compiler/backend/operands/
	cp src/Rz_compiler/frontend/semantics/*.class bin/Rz_compiler/frontend/semantics/
	cp src/Rz_compiler/frontend/syntax/*.class bin/Rz_compiler/frontend/syntax/
	cp src/Rz_compiler/frontend/semantics/identifier/*.class bin/Rz_compiler/frontend/semantics/identifier/
	cp src/Rz_compiler/backend/instructions/arithmetic_logic/*.class bin/Rz_compiler/backend/instructions/arithmetic_logic/
	cp src/Rz_compiler/backend/instructions/branch_jump/*.class bin/Rz_compiler/backend/instructions/branch_jump/
	cp src/Rz_compiler/backend/instructions/comparison/*.class bin/Rz_compiler/backend/instructions/comparison/
	cp src/Rz_compiler/backend/instructions/load_store_move/*.class bin/Rz_compiler/backend/instructions/load_store_move/
	cp src/Rz_compiler/backend/instructions/visitors/*.class bin/Rz_compiler/backend/instructions/visitors/
	$(MAKE) -C src clean
clean:
	$(MAKE) -C src clean
	-rm -rf bin